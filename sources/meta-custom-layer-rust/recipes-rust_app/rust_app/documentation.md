1. Clone the repo using `git clone git@github.com:smallB007/rust_gtk3_example.git`
2. cd to the repo using `cd rust_rust_gtk3_example`
3. build using `cargo build` to make sur it works

# install as specified [here](https://github.com/meta-rust/cargo-bitbake)

> I had to change cargo edition to 2018 because this is what I had installed and I don't know rust well enough to find out why it didn't want to install cargo bitbake otherwise.

5. cargo install --locked cargo-bitbake
6. generate yocto recipe using cargo bitbake `cargo bitbake`

this generate `rust_gtk3_example_0.1.0.bb` which i renamed to `rgtk3_0.1.0.bb`

I had to add some dependencies that made cargo unhappy when building
