# How to build/setup

1. clone this repo `git clone git@gitlab.com:frainville-public/yocto-rust-test.git`
2. cd to the repo `cd yocto-rust-test`
3. initialize submodules `git submodule init`
4. update submodules `git submodule update`
5. source poky `source sources/poky/oe-init-build-env`

# Build only the recipe

6. build the rgtk3 recipe `bitbake rgkt3`

> See sources/meta-custom-layer-rust/recipes-rust_app/rust_app/documentation.md for information how the bb recipe was created.

# Build the image with the recipe in it

Make sure you have the following line in `build/conf/local.conf` as described [here](https://wiki.yoctoproject.org/wiki/Cookbook:Example:Adding_packages_to_your_OS_image) in the yocto documentation.

```
CORE_IMAGE_EXTRA_INSTALL += "rgtk3"
```

7. then you can `bitbake core-image-minimal` to build the image that will contain your recipe
